#-------------------------------------------------
#
# Project created by QtCreator 2021-01-11T16:41:05
#
#-------------------------------------------------

#Core modules
QT       += core gui sql network xml script

#Bonus
#QT += multimedia phonon



greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt4librarytester
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
