#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QScriptEngine>
#include <QDomDocument>
#include <QTcpSocket>
#include <QSqlDatabase>
#include <QMessageBox>


////QAudioOutput is from Multimedia, VolumeSlider from Phonon
//#include <QAudioOutput>
//#include <VolumeSlider>
//void testextra() {
//    QAudioOutput a;
//    Phonon::VolumeSlider s;
//}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QScriptEngine script;
    QDomDocument xml;
    QTcpSocket network;


    //Database has a dynamic SQLite driver that won't cause compile-time errors
    if (!(QSqlDatabase::isDriverAvailable("QSQLITE"))) {
        QMessageBox::critical(this, "", "QSQLITE driver missing!");
    } else {
        QMessageBox::information(this, "", "All Qt libraries appear present");
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
